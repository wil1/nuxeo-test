Note: this requires enabling preview features on Gitpod (to enable sudo and Docker):

1. Open terminal

2. sudo docker-up

3. Open another terminal

4. docker-compose build

5. docker-compose up

6. The default user/password is Administrator/Administrator
